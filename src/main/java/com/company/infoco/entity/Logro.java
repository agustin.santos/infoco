package com.company.infoco.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "LOGRO", indexes = {
        @Index(name = "IDX_LOGRO_INDICADOR_ID", columnList = "INDICADOR_ID"),
        @Index(name = "IDX_LOGRO_ORGANISMO_ID", columnList = "ORGANISMO_ID"),
        @Index(name = "IDX_LOGRO_PROGRAM_ID", columnList = "PROGRAM_ID")
})
@Entity
public class Logro {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "PROGRAM_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Programa program;

    @JoinColumn(name = "INDICADOR_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Indicador indicador;

    @JoinColumn(name = "ORGANISMO_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Organismo organismo;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Logro.valor.validation.PositiveOrZero}")
    @Column(name = "VALOR_REALIZACION", nullable = false)
    @NotNull
    private Double valor_realizacion;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Logro.valor_resultado.validation.PositiveOrZero}")
    @Column(name = "VALOR_RESULTADO", nullable = false)
    @NotNull
    private Double valor_resultado;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Logro.importe.validation.PositiveOrZero}")
    @Column(name = "IMPORTE", nullable = false)
    @NotNull
    private Double importe;

    @PastOrPresent(message = "{msg://com.company.infoco.entity/Logro.fecha_valor.validation.PastOrPresent}")
    @Column(name = "FECHA_VALOR", nullable = false)
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date fecha_valor;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public Programa getProgram() {
        return program;
    }

    public void setProgram(Programa program) {
        this.program = program;
    }

    public Date getFecha_valor() {
        return fecha_valor;
    }

    public void setFecha_valor(Date fecha_valor) {
        this.fecha_valor = fecha_valor;
    }

    public Double getValor_resultado() {
        return valor_resultado;
    }

    public void setValor_resultado(Double valor_resultado) {
        this.valor_resultado = valor_resultado;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public Double getValor_realizacion() {
        return valor_realizacion;
    }

    public void setValor_realizacion(Double valor_realizacion) {
        this.valor_realizacion = valor_realizacion;
    }

    public Organismo getOrganismo() {
        return organismo;
    }

    public void setOrganismo(Organismo organismo) {
        this.organismo = organismo;
    }

    public Indicador getIndicador() {
        return indicador;
    }

    public void setIndicador(Indicador indicador) {
        this.indicador = indicador;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}