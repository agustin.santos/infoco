package com.company.infoco.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum TipoIndicador implements EnumClass<String> {

    RESULTADO("Resultado"),
    REALIZACIÓN("Realización");

    private String id;

    TipoIndicador(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static TipoIndicador fromId(String id) {
        for (TipoIndicador at : TipoIndicador.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}