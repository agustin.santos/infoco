package com.company.infoco.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.validation.group.UiCrossFieldChecks;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.groups.*;
import java.util.Date;
import java.util.UUID;

import com.company.infoco.validator.SpELAssert;
@JmixEntity
@Table(name = "PREVISION", indexes = {
        @Index(name = "IDX_PREVISION_INDICADOR_ID", columnList = "INDICADOR_ID"),
        @Index(name = "IDX_PREVISION_ORGANISMO_ID", columnList = "ORGANISMO_ID"),
        @Index(name = "IDX_PREVISION_PROGRAM_ID", columnList = "PROGRAM_ID")
})
@Entity
@SpELAssert(groups = {Default.class, UiCrossFieldChecks.class},
        value = "MED0001", //"valor_realizacion > 10",
        message = "{msg://com.company.infoco.entity/Prevision.valor_realizacion.validation.CustumValidation}")
public class Prevision {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "PROGRAM_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Programa program;

    @JoinColumn(name = "INDICADOR_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Indicador indicador;

    @JoinColumn(name = "ORGANISMO_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Organismo organismo;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Prevision.valor.validation.PositiveOrZero}")
    @Column(name = "VALOR_REALIZACION", nullable = false)
    @NotNull
    private Double valor_realizacion;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Prevision.valor_resultado.validation.PositiveOrZero}")
    @Column(name = "VALOR_RESULTADO", nullable = false)
    @NotNull
    private Double valor_resultado;

    @PositiveOrZero(message = "{msg://com.company.infoco.entity/Prevision.montante.validation.PositiveOrZero}")
    @Column(name = "MONTANTE")
    private Double montante;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public Programa getProgram() {
        return program;
    }

    public void setProgram(Programa program) {
        this.program = program;
    }

    public Double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        this.montante = montante;
    }

    public Double getValor_resultado() {
        return valor_resultado;
    }

    public void setValor_resultado(Double valor_resultado) {
        this.valor_resultado = valor_resultado;
    }

    public Double getValor_realizacion() {
        return valor_realizacion;
    }

    public void setValor_realizacion(Double valor_realizacion) {
        this.valor_realizacion = valor_realizacion;
    }

    public Organismo getOrganismo() {
        return organismo;
    }

    public void setOrganismo(Organismo organismo) {
        this.organismo = organismo;
    }

    public Indicador getIndicador() {
        return indicador;
    }

    public void setIndicador(Indicador indicador) {
        this.indicador = indicador;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}