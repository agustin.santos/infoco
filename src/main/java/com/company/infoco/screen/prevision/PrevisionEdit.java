package com.company.infoco.screen.prevision;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Prevision;

@UiController("Prevision.edit")
@UiDescriptor("prevision-edit.xml")
@EditedEntityContainer("previsionDc")
public class PrevisionEdit extends StandardEditor<Prevision> {
}