package com.company.infoco.screen.prevision;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Prevision;

@UiController("Prevision.browse")
@UiDescriptor("prevision-browse.xml")
@LookupComponent("previsionsTable")
public class PrevisionBrowse extends StandardLookup<Prevision> {
}