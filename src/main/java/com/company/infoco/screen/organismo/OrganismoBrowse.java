package com.company.infoco.screen.organismo;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Organismo;

@UiController("Organismo.browse")
@UiDescriptor("organismo-browse.xml")
@LookupComponent("organismoesTable")
public class OrganismoBrowse extends StandardLookup<Organismo> {
}