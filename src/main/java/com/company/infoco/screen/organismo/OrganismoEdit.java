package com.company.infoco.screen.organismo;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Organismo;

@UiController("Organismo.edit")
@UiDescriptor("organismo-edit.xml")
@EditedEntityContainer("organismoDc")
public class OrganismoEdit extends StandardEditor<Organismo> {
}