package com.company.infoco.screen.validation;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Validation;

@UiController("Validation.browse")
@UiDescriptor("validation-browse.xml")
@LookupComponent("validationsTable")
public class ValidationBrowse extends StandardLookup<Validation> {
}