package com.company.infoco.screen.validation;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Validation;

@UiController("Validation.edit")
@UiDescriptor("validation-edit.xml")
@EditedEntityContainer("validationDc")
public class ValidationEdit extends StandardEditor<Validation> {
}