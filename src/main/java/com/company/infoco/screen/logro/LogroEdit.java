package com.company.infoco.screen.logro;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Logro;

@UiController("Logro.edit")
@UiDescriptor("logro-edit.xml")
@EditedEntityContainer("logroDc")
public class LogroEdit extends StandardEditor<Logro> {
}