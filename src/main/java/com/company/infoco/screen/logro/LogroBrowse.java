package com.company.infoco.screen.logro;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Logro;

@UiController("Logro.browse")
@UiDescriptor("logro-browse.xml")
@LookupComponent("logroesTable")
public class LogroBrowse extends StandardLookup<Logro> {
}