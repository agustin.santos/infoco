package com.company.infoco.screen.indicador;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Indicador;

@UiController("Indicador.browse")
@UiDescriptor("indicador-browse.xml")
@LookupComponent("indicadorsTable")
public class IndicadorBrowse extends StandardLookup<Indicador> {
}