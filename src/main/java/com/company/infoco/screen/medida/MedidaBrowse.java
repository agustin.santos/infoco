package com.company.infoco.screen.medida;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Medida;

@UiController("Medida.browse")
@UiDescriptor("medida-browse.xml")
@LookupComponent("medidasTable")
public class MedidaBrowse extends StandardLookup<Medida> {
}