package com.company.infoco.screen.medida;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Medida;

@UiController("Medida.edit")
@UiDescriptor("medida-edit.xml")
@EditedEntityContainer("medidaDc")
public class MedidaEdit extends StandardEditor<Medida> {
}