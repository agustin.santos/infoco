package com.company.infoco.screen.programa;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Programa;

@UiController("Programa.browse")
@UiDescriptor("programa-browse.xml")
@LookupComponent("programasTable")
public class ProgramaBrowse extends StandardLookup<Programa> {
}