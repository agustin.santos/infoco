package com.company.infoco.screen.programa;

import io.jmix.ui.screen.*;
import com.company.infoco.entity.Programa;

@UiController("Programa.edit")
@UiDescriptor("programa-edit.xml")
@EditedEntityContainer("programaDc")
public class ProgramaEdit extends StandardEditor<Programa> {
}